package com.example.demo.services;

import com.example.demo.dtos.CustomerDto;
import com.example.demo.dtos.SearchDto;
import com.example.demo.helper.Indices;
import com.example.demo.models.Customer;
import com.example.demo.repositories.CustomerRepository;
import com.example.demo.utils.SearchUtil;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final RestHighLevelClient client;
    private static final ObjectMapper MAPPER = new ObjectMapper();
    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, RestHighLevelClient client){
        this.customerRepository = customerRepository;
        this.client = client;
    }

    @Override
    public Customer save(CustomerDto customerDto) {
        Customer customer = new Customer();
        customer.setId(customerDto.getId());
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        return customerRepository.save(customer);
    }

    @Override
    public Page<Customer> getAll() {
        return (Page<Customer>) customerRepository.findAll();
    }

    @Override
    public Customer getByFirstName(String firstName) {
        return customerRepository.findByFirstName(firstName);
    }

    @Override
    public Customer getById(Long id) {
        return customerRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteById(Long id) {
        customerRepository.deleteById(id);
    }

    @Override
    public List<Customer> search(SearchDto searchDto) {
        final SearchRequest request = SearchUtil.buildSearchRequest(Indices.CUSTOMER_INDEX,searchDto);

        if(request == null){
            return Collections.emptyList();
        }

        try{
            final SearchResponse response = client.search(request, RequestOptions.DEFAULT);
            final SearchHit[] searchHits = response.getHits().getHits();
            final List<Customer> customers = new ArrayList<>(searchHits.length);
            for(SearchHit hit:searchHits){
                customers.add(
                        MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                                .setVisibility(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY))
                                .readValue(hit.getSourceAsString(),Customer.class)
                );
            }
            return customers;
        }catch (Exception e){
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @Override
    public void createUpdate(List<CustomerDto> customerDtos) {
        List<Customer> createCustomers = new ArrayList<>();
        List<Customer> updateCustomers = new ArrayList<>();
        for(CustomerDto customerDto:customerDtos){
            Customer customer = customerRepository.findById(customerDto.getId()).orElse(null);
            if(customer == null){
                Customer newCustomer = new Customer();
                newCustomer.setId(customerDto.getId());
                newCustomer.setFirstName(customerDto.getFirstName());
                newCustomer.setLastName(customerDto.getLastName());
                createCustomers.add(newCustomer);
            }else{
                customer.setFirstName(customerDto.getFirstName());
                customer.setLastName(customerDto.getLastName());
                updateCustomers.add(customer);
            }
        }

        try{
            bulkCreate(createCustomers);
            bulkUpdate(updateCustomers);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void bulkCreate(List<Customer> customers) {
        customerRepository.saveAll(customers);
    }

    private void bulkUpdate(List<Customer> customers) {
        customerRepository.saveAll(customers);
    }
}
