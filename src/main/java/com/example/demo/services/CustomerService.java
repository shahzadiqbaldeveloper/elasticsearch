package com.example.demo.services;

import com.example.demo.dtos.CustomerDto;
import com.example.demo.dtos.SearchDto;
import com.example.demo.models.Customer;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CustomerService {

    Customer save(CustomerDto customerDto);

    Page<Customer> getAll();

    Customer getByFirstName(String firstName);

    Customer getById(Long id);

    void deleteById(Long id);

    List<Customer> search(SearchDto searchDto);

    void createUpdate(List<CustomerDto> customerDtos);
}
