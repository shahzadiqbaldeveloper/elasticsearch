package com.example.demo.controllers;

import com.example.demo.dtos.CustomerDto;
import com.example.demo.dtos.SearchDto;
import com.example.demo.models.Customer;
import com.example.demo.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
@CrossOrigin(origins = "*", maxAge = 3600)
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public Page<Customer> getAll(){
        return customerService.getAll();
    }

    @PostMapping
    public Customer save(@RequestBody CustomerDto customerDto){
        return customerService.save(customerDto);
    }

    @GetMapping("/{firstName}")
    public Customer getByFirstName(@PathVariable("firstName") String firstName){
        return customerService.getByFirstName(firstName);
    }

    @GetMapping("/getbyid/{id}")
    public Customer getById(@PathVariable("id") Long id){
        return customerService.getById(id);
    }

    @DeleteMapping("/deletebyid/{id}")
    public ResponseEntity deleteById(@PathVariable("id") Long id){
        customerService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/search")
    public List<Customer> search(@RequestBody SearchDto searchDto){
        return customerService.search(searchDto);
    }

    @PostMapping("/createupdate")
    public ResponseEntity createUpdate(@RequestBody List<CustomerDto> customerDtos){
        customerService.createUpdate(customerDtos);
        return new ResponseEntity(HttpStatus.OK);
    }
}
