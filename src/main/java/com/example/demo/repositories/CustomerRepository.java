package com.example.demo.repositories;

import com.example.demo.models.Customer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends ElasticsearchRepository<Customer,Long> {

    Customer findByFirstName(String firstName);

}
