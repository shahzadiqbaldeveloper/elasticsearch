package com.example.demo.utils;

import com.example.demo.dtos.SearchDto;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.util.CollectionUtils;

import java.util.List;

public class SearchUtil {

    public SearchUtil() {
    }

    public static SearchRequest buildSearchRequest(final String indexName,final SearchDto searchDto){
        try{
            final SearchSourceBuilder builder = new SearchSourceBuilder()
                    .postFilter(getQueryBuilder(searchDto));
            SearchRequest request = new SearchRequest(indexName);
            request.source(builder);

            return request;
        } catch (final Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public static QueryBuilder getQueryBuilder(final SearchDto searchDto){
        if(searchDto == null){
            return null;
        }

        final List<String> fields = searchDto.getFields();
        if(CollectionUtils.isEmpty(fields)){
            return null;
        }

        if(fields.size() > 1){
            MultiMatchQueryBuilder queryBuilder = QueryBuilders.multiMatchQuery(searchDto.getSearchTerm())
                    .type(MultiMatchQueryBuilder.Type.CROSS_FIELDS)
                    .operator(Operator.AND);

            fields.forEach(queryBuilder::field);

            return queryBuilder;
        }

        return fields.stream()
                .findFirst()
                .map(field ->
                        QueryBuilders.matchQuery(field,searchDto.getSearchTerm())
                            .operator(Operator.AND))
                .orElse(null);
    }
}
